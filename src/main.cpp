#include <iostream>
#include "easylogging++.h"
#include "BusEventManager.h"

INITIALIZE_EASYLOGGINGPP

void myCrashHandler(int sig){
    system("GGEC_BOOT_NOTIFY reset");
    el::Helpers::logCrashReason(sig, true);
    el::Helpers::crashAbort(sig);
}

int main(int argc, char **argv) {
    el::Helpers::setCrashHandler(myCrashHandler);

    if(argc<1 || !argv[1]){
        std::cout << "BusEventManager not enought param! Need volumeDB file path" << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << "BusEventManager Start!" << std::endl;

    auto manager = std::unique_ptr<GGEC::a113::bus::BusEventManager>(new GGEC::a113::bus::BusEventManager(argv[1]));

    manager->run();
    return EXIT_SUCCESS;
}