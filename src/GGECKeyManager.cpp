//
// Created by z on 18-1-24.
//

#include <fcntl.h>
#include <linux/input.h>
#include "GGECKeyManager.h"

#include <unistd.h>
#include "easylogging++.h"

namespace GGEC {
    namespace a113 {
        namespace key {

            GGECKeyManager::GGECKeyManager(void(*fp)(int keyCode, int pressTime)){
                call_key = fp;
            }

            bool GGECKeyManager::keyInit(const std::string keyDevPath) {

                m_keyFd = open(keyDevPath.c_str(), O_RDWR);//ghf-20171221
                if (m_keyFd < 0) {
                    LOG(ERROR) << "keyDevPath not persent, no any key event!!!";
                    return false;
                } else {
                    LOG(INFO) << "keyDevPath open success";
                }

                aip_key_thread = std::thread (&GGECKeyManager::do_aip_key_thread,this);
                //aip_key_thread.join();//join()等待子线程myThread执行完之后，主线程才可以继续执行下去，此时主线程会释放掉执行完后的子线程资源
                aip_key_thread.detach();//如果不想等待子线程，可以在主线程里面执行t1.detach()将子线程从主线程里分离，子线程执行完成后会自己释放掉资源。分离后的线程，主线程将对它没有控制权

                return true;
            }

            void GGECKeyManager::do_aip_key_thread(){
                LOG(INFO)<<"get into do_aip_key_thread";
                int keys_fd = m_keyFd;
                struct input_event t;

                struct timeval tbegin,tend;
                tbegin.tv_sec=0;
                tbegin.tv_usec=0;
                tend.tv_sec=0;
                tend.tv_usec=0;

                long keydowntime=0;


                while (1) {
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));

                    if (read (keys_fd, &t, sizeof (t)) == sizeof (t)) {
                        if (t.type == EV_KEY){

                            if ( (t.code==116) && (t.value == 1) )
                            {
                                tbegin=t.time;
                            }
                            else if (t.value == 0)
                            {
                                el::Loggers::getLogger("default")->info("##key %d %s\n", t.code, (t.value) ? "Pressed" : "Released");

                                if(t.code==114){
                                    call_key(114, 0);
//                                    m_Speaker->handleAdjustVolume(10);
                                }
                                else if(t.code==115){
//                                    m_Speaker->handleAdjustVolume(-10);
                                    call_key(115, 0);
                                }
                                else if(t.code==116){
                                    tend=t.time;
                                    keydowntime=(tend.tv_sec-tbegin.tv_sec)*1000000+(tend.tv_usec-tbegin.tv_usec);
                                    LOG(INFO) << "###keydowntime=" << keydowntime;

                                    if(keydowntime<1000000){
                                        call_key(116, keydowntime);

//                                        auto flag = !m_Speaker->getMicMuteState();
//                                        //setMicMuteFlag(flag);
//                                        m_Speaker->handleSetMicMute(flag);
//                                        m_triggerMicState = true;
//                                        if(flag){
//                                            system("aplay -Dhw:0,1 /usr/share/avs/med_state_privacy_mode_on.wav");
//                                        }
//                                        else{
//                                            system("aplay -Dhw:0,1 /usr/share/avs/med_state_privacy_mode_off.wav");
//                                        }
                                    }
                                }
                                else if(t.code==139){
                                    call_key(139, keydowntime);
//                                    auto flag = !m_Speaker->getMicMuteState();
//                                    if(flag){//if micmute,tapkey no use
//                                        m_tapkey=true;
//                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}