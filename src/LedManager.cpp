//
// Created by z on 18-1-24.
//


#include <easylogging++.h>
#include "LedManager.h"
#include "AVSState.h"

namespace GGEC {
    namespace a113 {
        namespace led {

            LedManager::LedManager(std::shared_ptr<GgecI2cCtrl> i2cCtrl)
                    : m_ggecI2cForLed(std::move(i2cCtrl)) {
            }

            void LedManager::setAVSState(char avsState, unsigned char subState) {
                if (GROUP_RESET == avsState) {
                    reset();
                } else if (GROUP_VOLUME == avsState) {
                    if(subState<0||subState>100) return;

                    ledCtrlIntf(GGEC_VOLUME_LED_SHOW, (unsigned char)((subState * NUM_OF_LED) / 100));
                } else if (GROUP_MIC == avsState) {
                    if (S_OFF == subState) {
                        m_isMicMute = true;
                        ledCtrlIntf(GGEC_MIC_OFF);
                    } else if (S_ON == subState) {
                        m_isMicMute = false;
                        ledCtrlIntf(GGEC_MIC_ON);
                    }
                } else if (GROUP_CONNECT == avsState) {
                    if (S_CONNECTING == subState) {
                        ledCtrlIntf(GGEC_ORANGE_LOADING);
                    } else if (S_CONNECTING_FAIL == subState) {
                        //TODO
                        ledCtrlIntf(GGEC_NET_UNCONNECTED);
                    } else if (S_CONNECTING_SUCCESS == subState) {
                        tryBackToIdle(true);
                    }
                } else if (GROUP_RECOGNIZE == avsState) {
                    if (S_LISTENING == subState) {
                        m_isSpeakingState = GGEC_LISTENING;
                        //TODO get direction
                        ledCtrlIntf(GGEC_LISTENING);
                    } else if (S_THINKING == subState) {
                        m_isSpeakingState = GGEC_THINKING;
                        ledCtrlIntf(GGEC_THINKING);
                    } else if (S_SPEAKING == subState) {
                        m_isSpeakingState = GGEC_SPEAKING;
                        ledCtrlIntf(GGEC_SPEAKING);
                    } else if (S_IDLE == subState) {
                        m_isSpeakingState = 0;
                        tryBackToIdle(true);
                    }
                } else if (GROUP_NOTIFICATION == avsState) {
                    if (S_ON == subState) {
                        m_notificationState = 0x01;
                        ledCtrlIntf(GGEC_BREATH, 3);
                    } else if (S_OFF == subState) {
                        m_notificationState = 0x00;
                        //TODO
                        tryBackToIdle(true);
                    }
                } else if (GROUP_WELCOME_TIPS == avsState) {
                    if (S_ON == subState) {
                        ledCtrlIntf(GGEC_WELCOME_TIPS);
                    } else if (S_OFF == subState) {
                        m_isSpeakingState = 0;
                        tryBackToIdle(true);
                    }
                } else if (GROUP_ERROR == avsState) {
                    if (S_ON == subState) {
                        m_isSpeakingState = GGEC_SYS_ERROR;
                        ledCtrlIntf(GGEC_SYS_ERROR);
                    } else if (S_OFF == subState) {
                        m_isSpeakingState = 0;
                        tryBackToIdle(true);
                    }
                }
            }

            bool LedManager::tryBackToIdle(bool needSetBack) {
                if (m_isMicMute || m_isSpeakingState != 0 || m_notificationState != 0) {
                    if(needSetBack){
                        if(m_isMicMute){
                            ledCtrlIntf(GGEC_MIC_OFF);
                        } else if(m_isSpeakingState != 0){
                            ledCtrlIntf(m_isSpeakingState);
                        } else if(m_notificationState != 0){
                            ledCtrlIntf(GGEC_BREATH, 3);
                        }
                    }
                    return false;
                } else {
                    ledCtrlIntf(GGEC_LED_RESET);
                    return true;
                }
            }


            void LedManager::reset() {
                m_notificationState = 0x00;
                m_isSpeakingState = 0;
                ledCtrlIntf(GGEC_LED_RESET);
            }

            void LedManager::ledCtrlIntf(int ledState, unsigned char num) {
                int delayTime = 0;
                switch (ledState) {

                    case GGEC_MIC_OFF:
                    case GGEC_BREATH:
                    case GGEC_BREATH_3TIMES: {
                        m_lastLedShowType = ledState;
                        m_lastLedShowNum = num;
                        delayTime = -1;
                        ledCtrlIntfImpl(ledState, num);
                    }
                        break;
                    case GGEC_LED_RESET:
                        m_isSpeakingState = 0;
                        if (m_isMicMute) {
                            m_lastLedShowType = GGEC_MIC_OFF;
                        } else if (m_notificationState != 0x00) {
                            m_lastLedShowType = GGEC_BREATH;
                            m_lastLedShowNum = 3;
                        } else {
                            m_lastLedShowType = GGEC_LED_RESET;
                            m_lastLedShowNum = 0;
                        }
                        delayTime = -1;
                        ledCtrlIntfImpl(m_lastLedShowType, m_lastLedShowNum);
                        break;
                    case GGEC_SPEAKING:
                    case GGEC_SYS_ERROR:
                        m_isSpeakingState = ledState;
                    case GGEC_LISTENING:
                    case GGEC_ACTIVE_LISTENING:
                    case GGEC_THINKING: {
                        m_lastLedShowType = ledState;
                        delayTime = -1;
                        ledCtrlIntfImpl(ledState, num);
                    }
                        break;
                    case GGEC_VOLUME_LED_SHOW: {
                        delayTime = 1000;
                        ledCtrlIntfImpl(ledState, num);
                    }
                        break;
                    case GGEC_MIC_ON: {
                        if (m_isSpeakingState != 0) {
                            m_lastLedShowType = m_isSpeakingState;
                        } else if (m_notificationState != 0x00) {
                            m_lastLedShowType = GGEC_BREATH;
                            m_lastLedShowNum = 3;
                        } else {
                            m_lastLedShowType = GGEC_LED_RESET;
                            m_lastLedShowNum = 0;
                        }
                        delayTime = -1;
                        ledCtrlIntfImpl(m_lastLedShowType, m_lastLedShowNum);
                    }
                        break;
                    case GGEC_WELCOME_TIPS:
                        ledState = GGEC_BREATH_3TIMES;
                        ledCtrlIntfImpl(ledState, 4);
                        break;
                    default:
                        ledCtrlIntfImpl(ledState, num);
                        break;
                }
                delayCall(delayTime);
            }

            void LedManager::ledCtrlIntfImpl(int ledState, unsigned char num) {
                m_ggecI2cForLed->ledCtrlIntfImpl(ledState, num);
            }

            bool LedManager::delayCall(int millisecond) {
                if (millisecond == 0) {
                    return true;
                }
                m_delayTimer.stop();
                if (millisecond < 0) {
                    LOG(INFO) << "delayCall reset !";
                    return true;
                }
                //        printf(LX("delayCall start").d("millisecond",millisecond));
                return m_delayTimer.start(std::chrono::milliseconds(millisecond), [this] {
                    ledCtrlIntfImpl(m_lastLedShowType, m_lastLedShowNum);
                }).valid();
            }

        }
    }
}