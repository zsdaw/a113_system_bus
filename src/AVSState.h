//
// Created by z on 18-1-25.
//

#ifndef GGEC_A113_EVENT_MANAGER_AVSSTATE_H
#define GGEC_A113_EVENT_MANAGER_AVSSTATE_H

namespace GGEC {
    namespace a113 {
        namespace led {

            ///// STATE ON,OF /////////
            const static char S_ON = 1;
            const static char S_OFF = 2;
            ///////////////////////////
            const static char GROUP_RESET = 0;

            const static char GROUP_VOLUME = 1; // volume 0 - 100

            const static char GROUP_MIC = 2; // subState : on / of

            const static char GROUP_CONNECT = 3; // 1 connecting, 2 success ,3 fail
            const static char S_CONNECTING = 1;
            const static char S_CONNECTING_SUCCESS = 2;
            const static char S_CONNECTING_FAIL = 3;

            /////// GROUP RECOGNIZE /////
            const static char GROUP_RECOGNIZE = 4;
            const static char S_LISTENING = 1;
            const static char S_THINKING = 2;
            const static char S_SPEAKING = 3;
            const static char S_IDLE = 4;
            /////// GROUP RECOGNIZE END /////


            const static char GROUP_NOTIFICATION = 5;  // subState : on / of

            const static char GROUP_WELCOME_TIPS = 6;  // subState : on / of


            const static char GROUP_ERROR = 13; // subState : on / of

        }
    }
}

#endif //GGEC_A113_EVENT_MANAGER_AVSSTATE_H
