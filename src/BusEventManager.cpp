//
// Created by z on 18-1-24.
//

#include "BusEventManager.h"
#include "AVSState.h"
#include "easylogging++.h"


namespace GGEC {
    namespace a113 {
        namespace bus {

            BusEventManager *bem = nullptr;

            void notify_key(int keyCode, int pressTime) {
                bem->onkey(keyCode, pressTime);
            }


            BusEventManager::BusEventManager(const std::string & filePath) :
                    m_ggecI2cForLed(GgecI2cCtrl::create("/dev/i2c-0")),
                    mSpeakerManager(GgecI2cCtrl::create("/dev/i2c-2"), filePath),
                    mGgecKeyManager(notify_key),
                    mLedManager(m_ggecI2cForLed) {
                bem = this;
                mGgecKeyManager.keyInit("/dev/input/event0");
            }

            int BusEventManager::run() {
                DBusError dberr;
                DBusMessage *msg;

                dbus_error_init(&dberr);
                m_dbusConnect = dbus_bus_get_private(DBUS_BUS_SESSION, &dberr);

                if(m_dbusConnect== nullptr){
                    LOG(ERROR) << "gm_dbusConnect is null, message: "<<dberr.message;
                } else{
                    LOG(INFO) << "gm_dbusConnect not null ";
                }
                dbus_bus_request_name(m_dbusConnect, CONNECT_NAME, DBUS_NAME_FLAG_REPLACE_EXISTING, &dberr);


                if (dbus_error_is_set(&dberr)) {
                    LOG(ERROR) << "getting session bus failed: "<<dberr.message;
                    dbus_error_free(&dberr);
                    return -1;
                }

                m_ggecI2cForLed->setBusConnect(m_dbusConnect);

                LOG(INFO) <<"getting session bus begin";
                while (true) {
                    dbus_connection_read_write(m_dbusConnect, 0);
                    msg = dbus_connection_pop_message(m_dbusConnect);

                    if (msg == NULL) {
                        std::this_thread::sleep_for(std::chrono::milliseconds(5));
                        continue;
                    }

                    handleDBusMessage(msg);

                }
            }

            DBusHandlerResult BusEventManager::handleDBusMessage(DBusMessage *message) {

                bool result;
                if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_GET_VOLUME)) {
                    result = handleGetVolume(message);
                } else if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_SET_VOLUME)) {
                    result = handleSetVolume(message);
                } else if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_ADJUST_VOLUME)) {
                    result = handleAdjustVolume(message);
                } else if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_GET_SPEAKER_MUTE)) {
                    result = handleGetSpeakerMute(message);
                } else if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_SET_SPEAKER_MUTE)) {
                    result = handleSetSpeakerMute(message);
                } else if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_GET_MIC_MUTE)) {
                    result = handleGetMicMute(message);
                } else if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_SET_MIC_MUTE)) {
                    result = handleSetMicMute(message);
                } else if (dbus_message_is_method_call(message, CONNECT_NAME, METHOD_SET_AVS_STATE)) {
                    result = handleSetAVSState(message);
                } else {
                    LOG(INFO) << "receive other bus message "
                              << dbus_message_get_path(message) << " "
                              << dbus_message_get_destination(message);
                    result = false;
                    dbus_message_unref(message);
                }

                return result ? DBUS_HANDLER_RESULT_HANDLED : DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
            }

            bool BusEventManager::handleGetVolume(DBusMessage *message) {
                LOG(INFO) << "handleGetVolume";
                int curren_volume = mSpeakerManager.getCurrentVolume();

                DBusMessage *reply;
                reply = dbus_message_new_method_return(message);
                dbus_message_append_args(reply, DBUS_TYPE_INT32, &curren_volume, DBUS_TYPE_INVALID);
                dbus_connection_send(m_dbusConnect, reply, NULL);
                dbus_message_unref(reply);
                dbus_message_unref(message);
                return true;
            }

            bool BusEventManager::handleSetVolume(DBusMessage *message) {
                int who = 0;
                int volume = 0;
                int showLed = 1;
                dbus_message_get_args(message, NULL, DBUS_TYPE_BYTE, &who, DBUS_TYPE_INT32, &volume, DBUS_TYPE_INT32,
                                      &showLed, DBUS_TYPE_INVALID);

                LOG(INFO) <<"handleSetVolume who:" << who << " vol:" << volume << " showLed:" <<showLed;

                int res = mSpeakerManager.setVolume(volume) ? STATE_SUCCESS : STATE_FAIL;

                DBusMessage *reply;
                reply = dbus_message_new_method_return(message);
                dbus_message_append_args(reply, DBUS_TYPE_INT32, &res, DBUS_TYPE_INVALID);
                dbus_connection_send(m_dbusConnect, reply, NULL);
                dbus_message_unref(reply);
                dbus_message_unref(message);


                if (res == STATE_SUCCESS) {
                    if(showLed==1) {
                        mLedManager.setAVSState(led::GROUP_VOLUME, volume);
                    }
                    notifyVolumeChange(volume, who);
                }

                return true;
            }

            bool BusEventManager::handleAdjustVolume(DBusMessage *message) {
                int who = 0;
                int adjvolume = 0;
                int currentVolume;
                int showLed = 1;
                dbus_message_get_args(message, NULL, DBUS_TYPE_BYTE, &who, DBUS_TYPE_INT32, &adjvolume,
                                      DBUS_TYPE_INT32, &showLed, DBUS_TYPE_INVALID);

                LOG(INFO) <<"handleAdjustVolume who:" << who << " adjvol:" << adjvolume << " showLed:" <<showLed;

                int res = mSpeakerManager.adjustVolume(adjvolume, &currentVolume) ? STATE_SUCCESS : STATE_FAIL;

                DBusMessage *reply;
                reply = dbus_message_new_method_return(message);
                dbus_message_append_args(reply, DBUS_TYPE_INT32, &res, DBUS_TYPE_INVALID);
                dbus_connection_send(m_dbusConnect, reply, NULL);
                dbus_message_unref(reply);
                dbus_message_unref(message);

                if (res == STATE_SUCCESS) {
                    if(showLed == 1) {
                        mLedManager.setAVSState(led::GROUP_VOLUME, currentVolume);
                    }
                    DBusMessage *notify = dbus_message_new_signal(CONNECT_PATH, CONNECT_NAME, SIGNAL_VOLUME_CHANGE);
                    dbus_message_append_args(notify, DBUS_TYPE_BYTE, &who, DBUS_TYPE_INT32, &currentVolume,
                                             DBUS_TYPE_INVALID);
                    sendSignal(notify);
                }

                return true;
            }

            bool BusEventManager::handleGetSpeakerMute(DBusMessage *message) {
                LOG(INFO) << "handleGetSpeakerMute bus message";
                int isSpeakerMute = mSpeakerManager.getIsSpeakerMute() ? 1 : 0;

                DBusMessage *reply;
                reply = dbus_message_new_method_return(message);
                dbus_message_append_args(reply, DBUS_TYPE_INT32, &isSpeakerMute, DBUS_TYPE_INVALID);
                dbus_connection_send(m_dbusConnect, reply, NULL);
                dbus_message_unref(reply);
                dbus_message_unref(message);
                return true;
            }

            bool BusEventManager::handleSetSpeakerMute(DBusMessage *message) {

                int who = 0;
                int muteSpeaker = 0;
                dbus_message_get_args(message, NULL, DBUS_TYPE_BYTE, &who, DBUS_TYPE_INT32, &muteSpeaker,
                                      DBUS_TYPE_INVALID);
                LOG(INFO) << "handleSetSpeakerMute bus message, who: " << who << " muteSpeaker:"<<muteSpeaker;

                int res = mSpeakerManager.muteSpeaker(muteSpeaker == 1) ? STATE_SUCCESS : STATE_FAIL;


                DBusMessage *reply;
                reply = dbus_message_new_method_return(message);
                dbus_message_append_args(reply, DBUS_TYPE_INT32, &res, DBUS_TYPE_INVALID);
                dbus_connection_send(m_dbusConnect, reply, NULL);
                dbus_message_unref(reply);
                dbus_message_unref(message);

                if (res == STATE_SUCCESS) {
                    DBusMessage *notify = dbus_message_new_signal(CONNECT_PATH, CONNECT_NAME,
                                                                  SIGNAL_SPEAKER_MUTE_CHANGE);
                    dbus_message_append_args(notify, DBUS_TYPE_BYTE, &who, DBUS_TYPE_INT32, &muteSpeaker,
                                             DBUS_TYPE_INVALID);
                    sendSignal(notify);
                }

                return true;
            }

            bool BusEventManager::handleGetMicMute(DBusMessage *message) {

                bool bMute = mSpeakerManager.getMicIsMute();
                int isMicMute = bMute? 1:0;
                mLedManager.setMicState(bMute);
                LOG(INFO) << "handleGetMicMute isMute="<<isMicMute;

                DBusMessage *reply;
                reply = dbus_message_new_method_return(message);
                dbus_message_append_args(reply, DBUS_TYPE_INT32, &isMicMute, DBUS_TYPE_INVALID);
                dbus_connection_send(m_dbusConnect, reply, NULL);
                dbus_message_unref(reply);
                dbus_message_unref(message);
                return true;
            }

            bool BusEventManager::handleSetMicMute(DBusMessage *message) {
                LOG(INFO) <<"handleSetMicMute not use!!";
                return false;
//                int who = 0;
//                int muteMic = 0;
//                dbus_message_get_args(message, NULL, DBUS_TYPE_INT32, &who, DBUS_TYPE_INT32, &muteMic,
//                                      DBUS_TYPE_INVALID);
//                printf("handleSetMicMute bus message, who:%d mute:%d\n", who, muteMic);
//                //TODO set mic mute
//                int res = 0;
//
//
//                DBusMessage *reply;
//                reply = dbus_message_new_method_return(message);
//                dbus_message_append_args(reply, DBUS_TYPE_INT32, &res, DBUS_TYPE_INVALID);
//                dbus_connection_send(m_dbusConnect, reply, NULL);
//                dbus_message_unref(reply);
//                dbus_message_unref(message);
//
//                if (res == STATE_SUCCESS) {
//                    DBusMessage *notify = dbus_message_new_signal(CONNECT_PATH, CONNECT_NAME, SIGNAL_MIC_MUTE_CHANGE);
//                    dbus_message_append_args(notify, DBUS_TYPE_INT32, &who, DBUS_TYPE_INT32, &muteMic,
//                                             DBUS_TYPE_INVALID);
//                    sendSignal(notify);
//                }
//
//                return true;
            }

            bool BusEventManager::handleSetAVSState(DBusMessage *message) {
                int who = 0;
                int avsState = 0;
                int param1 = 0;
                dbus_message_get_args(message, NULL, DBUS_TYPE_BYTE, &who, DBUS_TYPE_BYTE, &avsState,DBUS_TYPE_BYTE, &param1,
                                      DBUS_TYPE_INVALID);
                LOG(INFO) << "handleSetAVSState bus message, who:"<<who<<" avsState:"<<avsState<<" param:"<<param1;
                int res = STATE_SUCCESS;

                DBusMessage *reply;
                reply = dbus_message_new_method_return(message);
                dbus_message_append_args(reply, DBUS_TYPE_INT32, &res, DBUS_TYPE_INVALID);
                dbus_connection_send(m_dbusConnect, reply, NULL);
                dbus_message_unref(reply);
                dbus_message_unref(message);
                // change state
                mLedManager.setAVSState(avsState, param1);
                return true;
            }

            bool BusEventManager::sendSignal(DBusMessage *message) {
                if (message == nullptr) {
                    LOG(ERROR) << "sendSignal message is null";
                    return false;
                }
                if (!dbus_connection_send(m_dbusConnect, message, NULL)) {
                    LOG(ERROR) << "sendSignal Out Of Memory!";
                    return false;
                }
                dbus_connection_flush(m_dbusConnect);
                return true;
            }

            bool BusEventManager::notifyVolumeChange(int volume, char who) {
                DBusMessage *notify = dbus_message_new_signal(CONNECT_PATH, CONNECT_NAME, SIGNAL_VOLUME_CHANGE);
                dbus_message_append_args(notify, DBUS_TYPE_BYTE, &who, DBUS_TYPE_INT32, &volume,
                                         DBUS_TYPE_INVALID);
                sendSignal(notify);
            }

            bool BusEventManager::notifyMicMuteStateChange(bool micMute, char who) {
                int isMute = micMute ? 1 : 0;
                DBusMessage *notify = dbus_message_new_signal(CONNECT_PATH, CONNECT_NAME, SIGNAL_MIC_MUTE_CHANGE);
                dbus_message_append_args(notify, DBUS_TYPE_BYTE, &who, DBUS_TYPE_INT32, &isMute,
                                         DBUS_TYPE_INVALID);
                sendSignal(notify);
            }

            bool BusEventManager::notifyTriggerEvent() {
                DBusMessage *notify = dbus_message_new_signal(CONNECT_PATH, CONNECT_NAME, SIGNAL_TRIGGRT_KEY_PRESS);
                dbus_message_append_args(notify, DBUS_TYPE_INVALID);
                sendSignal(notify);
            }


            void BusEventManager::onkey(int keyCode, int pressTime) {
                if (114 == keyCode) { // volume up
                    int currentVolume;
                    mSpeakerManager.adjustVolume(10, &currentVolume);
                    notifyVolumeChange(currentVolume);
                    mLedManager.setAVSState(led::GROUP_VOLUME, currentVolume);
                } else if (115 == keyCode) { // vol down
                    int currentVolume;
                    mSpeakerManager.adjustVolume(-10, &currentVolume);
                    mLedManager.setAVSState(led::GROUP_VOLUME, currentVolume);
                    notifyVolumeChange(currentVolume);
                } else if (116 == keyCode) { // mute
                    bool isMicMute = mSpeakerManager.getMicIsMute();
                    mSpeakerManager.handleSetMicMute(isMicMute? 0 : 1);

                    if (isMicMute) {
                        mLedManager.setAVSState(led::GROUP_MIC, led::S_ON);
                        system("aplay -Dhw:0,1 /usr/share/avs/med_state_privacy_mode_on.wav");
                    } else {
                        mLedManager.setAVSState(led::GROUP_MIC, led::S_OFF);
                        system("aplay -Dhw:0,1 /usr/share/avs/med_state_privacy_mode_off.wav");
                    }
                    notifyMicMuteStateChange(!isMicMute, W_System);
                } else if (139 == keyCode) {
                    // key trigger
                    notifyTriggerEvent();
                }
            }



        }
    }
}