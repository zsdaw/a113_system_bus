
#include <sql/ggecSqlite.h>
#include <sql/SQLiteUtils.h>
#include <sql/SQLiteStatement.h>
#include <easylogging++.h>

namespace alexaClientSDK {
namespace sampleApp {
	
	using namespace alexaClientSDK::storage::sqliteStorage;
	
	
	static const int GGEC_INIT_VOLUME = 60;
	
	static const std::string DATABASE_COLUMN_ID_NAME = "id";
	
	static const std::string GGEC_VOLUME_TABLE_NAME = "ggec_volume";
	
	static const std::string CREATE_VOLUME_TABLE_SQL_STRING = std::string("CREATE TABLE ") +
        GGEC_VOLUME_TABLE_NAME + " (" +
        DATABASE_COLUMN_ID_NAME + " INT PRIMARY KEY NOT NULL," +
        "volume_value INT NOT NULL," + 
		"isMute INT NOT NULL);";



	static bool createVolumeTable(sqlite3* dbHandle) {
		if (!performQuery(dbHandle, CREATE_VOLUME_TABLE_SQL_STRING)) {
			LOG(ERROR) << "ggecSqlite#createAlertsTableFailed. Table could not be created.";
			return false;
		}

		return true;
	}
	
	bool ggecSqlite::initializeSqlite(std::string storageFilePath) {

		if (storageFilePath.empty()) {
			LOG(ERROR) << "ggecSqlite#initializeAlertsFailed. could not load ggecSqlite storage file path.";
			return false;
		}
		
		if (!open(storageFilePath)) {
            LOG(INFO) << "ggecSqlite#initialize. storage file does not exist.  Creating.";
			if (!createDatabase(storageFilePath)) {
				LOG(ERROR) << "ggecSqlite#initializeFailed.Could not create database file.";
				return false;
			}
			store(GGEC_INIT_VOLUME);
		}
		
		if((getVolume() < 0) || (getVolume() > 100)) {
			modifyVolume(GGEC_INIT_VOLUME);
		}
		
		return true;
		
	}
	
	ggecSqlite::ggecSqlite(const std::string & filePath){
		m_dbHandle = nullptr;
		m_id = 0;
		m_volume = -1;
		initializeSqlite(filePath);
	}
	
	bool ggecSqlite::open(const std::string & filePath) {
		if (m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#openFailed.Database handle is already open.";
			return false;
		}

		m_dbHandle = openSQLiteDatabase(filePath);
		if (!m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#openFailed.Database could not be opened. file path:"<< filePath;
			return false;
		}

		return true;
	}

	bool ggecSqlite::createDatabase(const std::string& filePath) {
		
		if (m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#createDatabaseFailed.Database handle is already open.";
			return false;
		}
		
		m_dbHandle = createSQLiteDatabase(filePath);
		if (!m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#createDatabaseFailed. Database could not be created. file path:"<< filePath;
			return false;
		}
		
		if (!createVolumeTable(m_dbHandle)) {
			LOG(ERROR) << "ggecSqlite#createDatabaseFailed. Alerts table could not be created.";
			close();
			return false;
		}
		
		return true;
		
    }
	
	bool ggecSqlite::isOpen() {
		return (nullptr != m_dbHandle);
	}

	void ggecSqlite::close() {
		if (m_dbHandle) {
			closeSQLiteDatabase(m_dbHandle);
			m_dbHandle = nullptr;
		}
	}
	
	bool ggecSqlite::store(int volume, int isMute) {
		if (!m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#storeFailed. Database handle is not open.";
			return false;
		}

		std::unique_lock<std::mutex> lock{m_mutex};
		const std::string sqlString = "INSERT INTO " + GGEC_VOLUME_TABLE_NAME + " (" +
								"id, volume_value, isMute"
								") VALUES (" +
								"?, ?, ?" +
								");";

		int id = 0;
		if (!getTableMaxIntValue(m_dbHandle, GGEC_VOLUME_TABLE_NAME, DATABASE_COLUMN_ID_NAME, &id)) {
			LOG(ERROR) << "ggecSqlite#storeFailed. Cannot generate ggecSqlite id.";
			return false;
		}
		id++;

		SQLiteStatement statement(m_dbHandle, sqlString);

		if (!statement.isValid()) {
			LOG(ERROR) << "ggecSqlite#storeFailed. Could not create statement.";
			return false;
		}

		int boundParam = 1;
		if (!statement.bindIntParameter(boundParam++, id) ||
			!statement.bindIntParameter(boundParam++, volume) ||
			!statement.bindIntParameter(boundParam++, isMute)) {
			LOG(ERROR) << "ggecSqlite#storeFailed. Could not bind parameter.";
			return false;
		}

		if (!statement.step()) {
			LOG(ERROR) << "ggecSqlite#storeFailed. Could not perform step.";
			return false;
		}

		statement.finalize();
		lock.unlock();

		return true;
	}
	
	bool ggecSqlite::loadHelper() {
		if (!m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#loadHelperFailed. Database handle is not open.";
			return false;
		}

		std::unique_lock<std::mutex> lock{m_mutex};
		const std::string sqlString = "SELECT * FROM " + GGEC_VOLUME_TABLE_NAME + ";";

		SQLiteStatement statement(m_dbHandle, sqlString);

		if (!statement.isValid()) {
			LOG(ERROR) << "ggecSqlite#loadHelperFailed. Could not create statement.";
			return false;
		}

		if (!statement.step()) {
			LOG(ERROR) << "ggecSqlite#loadHelperFailed. Could not perform step.";
			return false;
		}

		while (SQLITE_ROW == statement.getStepResult()) {
			int numberColumns = statement.getColumnCount();

			// SQLite cannot guarantee the order of the columns in a given row, so this logic is required.
			for (int i = 0; i < numberColumns; i++) {
				std::string columnName = statement.getColumnName(i);

				if ("id" == columnName) {
					m_id = statement.getColumnInt(i);
				} else if ("volume_value" == columnName) {
					m_volume = statement.getColumnInt(i);
				} else if ("isMute" == columnName) {
					m_isMute = statement.getColumnInt(i);
				}
			
			}
			statement.step();
		}

		statement.finalize();
		
		lock.unlock();

		return true;
	}

	int ggecSqlite::getVolume() {
		loadHelper();
		return m_volume;
	}
	
	int ggecSqlite::getMuteState() {
		loadHelper();
		return m_isMute;
	}
	
	bool ggecSqlite::modifyVolume(int volume) {
		if (!m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#modifyFailed, Database handle is not open.";
			return false;
		}

		std::unique_lock<std::mutex> lock{m_mutex};
		const std::string sqlString = "UPDATE " + GGEC_VOLUME_TABLE_NAME + " SET " +
								"volume_value=? " +
								"WHERE id=?;";

		SQLiteStatement statement(m_dbHandle, sqlString);

		if (!statement.isValid()) {
			LOG(ERROR) << "ggecSqlite#modifyFailed,Could not create statement.";
			return false;
		}

		int boundParam = 1;
		if (!statement.bindIntParameter(boundParam++, volume) ||
			!statement.bindIntParameter(boundParam++, m_id)) {
			LOG(ERROR) << "ggecSqlite#modifyFailed,Could not bind a parameter.";
			return false;
		}

		if (!statement.step()) {
			LOG(ERROR) << "ggecSqlite#modifyFailed, Could not perform step.";
			return false;
		}
		
		lock.unlock();

		return true;
	}
	
	bool ggecSqlite::modifyMuteState(int isMute) {
		if (!m_dbHandle) {
			LOG(ERROR) << "ggecSqlite#modifyFailed, Database handle is not open.";
			return false;
		}
		
		std::unique_lock<std::mutex> lock{m_mutex};

		const std::string sqlString = "UPDATE " + GGEC_VOLUME_TABLE_NAME + " SET " +
								"isMute=? " +
								"WHERE id=?;";

		SQLiteStatement statement(m_dbHandle, sqlString);

		if (!statement.isValid()) {
			LOG(ERROR) << "ggecSqlite#modifyFailed, Could not create statement.";
			return false;
		}

		int boundParam = 1;
		if (!statement.bindIntParameter(boundParam++, isMute) ||
			!statement.bindIntParameter(boundParam++, m_id)) {
			LOG(ERROR) << "ggecSqlite#modifyFailed Could not bind a parameter.";
			return false;
		}

		if (!statement.step()) {
			LOG(ERROR) << "ggecSqlite#modifyFailed Could not perform step.";
			return false;
		}
		
		lock.unlock();

		return true;
	}

	bool ggecSqlite::erase() {
		
		std::unique_lock<std::mutex> lock{m_mutex};
		const std::string sqlString = "DELETE FROM " + GGEC_VOLUME_TABLE_NAME + " WHERE id=?;";

		SQLiteStatement statement(m_dbHandle, sqlString);

		if (!statement.isValid()) {
			LOG(ERROR) << "ggecSqlite#eraseFailed Could not create statement.";
			return false;
		}

		int boundParam = 1;
		if (!statement.bindIntParameter(boundParam, m_id)) {
			LOG(ERROR) << "ggecSqlite#eraseFailed Could not bind a parameter.";
			return false;
		}

		if (!statement.step()) {
			LOG(ERROR) << "ggecSqlite#eraseFailed Could not perform step.";
			return false;
		}
		
		lock.unlock();

		return true;
	}
	
}
}