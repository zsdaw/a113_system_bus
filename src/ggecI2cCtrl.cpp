
#include <easylogging++.h>
#include "ggecI2cCtrl.h"

namespace GGEC {
namespace a113 {

    #define USE_I2C_DEV 1 // TODO 1 set enable i2c, pc need set to zero

	#define CFG_META_SWITCH (255)
	#define CFG_META_DELAY  (254)
	#define CFG_META_BURST  (253)

	cfg_reg tas5754_registers[] = {
		//{0x3d,0x60},
		//{0x3e,0x60},
		{0x08,0x20},
		{0x55,0x07},
		{0x07,0x01},
	};


	std::shared_ptr<GgecI2cCtrl> GgecI2cCtrl::create(const std::string i2cDevPath) {
		auto ggecI2cCtrl = std::shared_ptr<GgecI2cCtrl> (new GgecI2cCtrl);

		if(USE_I2C_DEV) {
			if (!ggecI2cCtrl->i2cInit(i2cDevPath)) {
                LOG(ERROR) << "i2c Initialization error";
				return nullptr;
			}
		}

		return ggecI2cCtrl;

	}

	void GgecI2cCtrl::setBusConnect(DBusConnection * conn){
		m_dbusConnect = conn;
	}

	bool GgecI2cCtrl::i2cInit(std::string i2cDevPath) {

		m_i2cFd = open(i2cDevPath.c_str(), O_RDWR);
		if(m_i2cFd < 0) {
            LOG(ERROR) << "i2c is not present";
			return false;
		};
		g_i2c_buf = NULL;
		g_cbMaxI2cWrite = DEF_MAX_I2C_WRITE_LEN;
		g_cbMaxI2cRead = DEF_MAX_I2C_READ_LEN;
		return true;

	}

	void GgecI2cCtrl::tas5754Init() {
		//auto storageVolume = m_ggecSqlite->getVolume();
		//unsigned char mapVolume = (unsigned char)(GGEC_VOLUME_TOTAL_MAP - storageVolume);
		//cfg_reg tas5754_vol_registers[] = {
		//	{0x3d, mapVolume},
		//	{0x3e, mapVolume},
		//	{0x08, 0x20},
		//	{0x55, 0x07},
		//	{0x07, 0x01},
		//};
		transmit_registers(TAS5754_SLAVE_ADDR, tas5754_registers, sizeof(tas5754_registers)/sizeof(tas5754_registers[0]));
        LOG(INFO) << "GgecI2cCtrl::tas5754Init";
	}

	int GgecI2cCtrl::i2cWrite(int slaveAddr, int regAddr, unsigned int *buffer, int size, int g_bAddressOffset) {

		struct i2c_rdwr_ioctl_data packets;
		struct i2c_msg msg[1];
		unsigned char* write_buf = (unsigned char*) buffer;
		unsigned int write_len = size * 4;
		int fd = m_i2cFd;
		int ret = 0;
		int i;

		if (g_i2c_buf == NULL) {
			g_i2c_buf = (char*)malloc(g_cbMaxI2cWrite + g_bAddressOffset);
			if (g_i2c_buf == NULL) {
				return -ERRNO_NOMEM;
			}
		}

		for(i=0; i<g_bAddressOffset;i++)
			g_i2c_buf[i] = (unsigned char)(regAddr >> ((g_bAddressOffset -i -1)*8));

		memcpy(&g_i2c_buf[i], write_buf, write_len);

		msg[0].addr = slaveAddr;
		msg[0].flags = 0; //7 bits
		msg[0].len = write_len + g_bAddressOffset;
		msg[0].buf = (uint8_t*) g_i2c_buf;

		packets.msgs = msg;
		packets.nmsgs = 1;

		//printf("I2C: <START> %02X", slaveAddr<<1);
        //for(i=0;i<msg[0].len ;i++)
        //{
        //    printf(" %02X",(0xff& g_i2c_buf[i]) );
        //}
        //printf(" <STOP>");

		std::unique_lock<std::mutex> lock{m_mutex};
		ret = ioctl(fd, I2C_RDWR, &packets);
		lock.unlock();

		if (ret < 0) {
            LOG(ERROR) << "i2c_write error";
			return -1;
		}
		return 0;
	}

	int GgecI2cCtrl::i2cRead(int slaveAddr, int regAddr, unsigned int *buffer, int size, int g_bAddressOffset) {
        if(USE_I2C_DEV) {
			struct i2c_rdwr_ioctl_data packets;
            struct i2c_msg msg[2];
            unsigned char *read_buf = (unsigned char *) buffer;
            unsigned int read_len = size * 4;
            int fd = m_i2cFd;
            unsigned long be_sub_addr = htobe32(regAddr);
			int ret = 0;
            int i;

			msg[0].addr = slaveAddr;
			msg[0].flags = 0; //7 bits
			msg[0].len = g_bAddressOffset;
			msg[0].buf = (uint8_t *) &be_sub_addr;

			for (i = 0; i < g_bAddressOffset; i++)
				msg[0].buf[i] = (unsigned char) (regAddr >> ((g_bAddressOffset - i - 1) * 8));

			msg[1].addr = slaveAddr;
			msg[1].flags = I2C_M_RD; //read flag
			msg[1].len = read_len;
			msg[1].buf = read_buf;

			packets.msgs = msg;
			packets.nmsgs = 2;

			std::unique_lock<std::mutex> lock{m_mutex};
            ret = ioctl(fd, I2C_RDWR, &packets);
			lock.unlock();

			//printf("I2C: <START> %02X", slaveAddr<<1);
			//for (i=0;i<msg[0].len ;i++)
			//    printf(" %02X", msg[0].buf[i] );
			//
			//printf(" <RESTART> %02X [", slaveAddr<<1 |1);
			//
			//for (i=0;i<msg[1].len;i++) {
			//    if(i==0)
			//        printf("%02X", msg[1].buf[i]);
			//    else
			//        printf("%02X", msg[1].buf[i]);
			//}
			//printf("] <STOP>");

			if (ret < 0) {
                LOG(ERROR) << "i2c_read error.";
				return -1;
			}
		}
		return 0;
	}

	int GgecI2cCtrl::SendCmd0 (Command *cmd) {
		return SendCmd_i2c(cmd);
	}

	int GgecI2cCtrl::SendCmd1 (Command *cmd, uint32_t  app_module_id, uint32_t  command_id, uint32_t num_32b_words, ...) {
		va_list     args;
		uint32_t    n         ;

		va_start (args , num_32b_words) ;

		// at least two words of header
		if (num_32b_words > MAX_COMMAND_SIZE)
		{
			va_end (args) ;
			return (SENDCMD_RC_INVALID_ARG_NUM);
		}
		cmd->num_32b_words = (command_id&CMD_GET(0)) ? MAX_COMMAND_SIZE : num_32b_words;
		cmd->command_id    = command_id;
		cmd->reply         = 0;
		cmd->app_module_id = app_module_id;

		for (n = 0 ; n < num_32b_words ; n++)
		{
			cmd->data[n] = va_arg(args, int32_t);
		}
		va_end (args) ;

		return SendCmd0(cmd);
	}

	int GgecI2cCtrl::SendCmd_i2c(Command *cmd) {

		int num_32b_words = cmd->num_32b_words;

		cmd->num_32b_words = (cmd->command_id&CMD_GET(0)) ? MAX_COMMAND_SIZE : num_32b_words;

		unsigned int *i2c_data = (unsigned int *)cmd;
		int size = num_32b_words + 2;

		// write words 1 to N-1 , to addresses 4 to 4+4*N-1
		i2cWrite(CONX_SLAVE_ADDR, 0x4, &i2c_data[1], size-1, 2);

		// write word 0 to address 0
		i2cWrite(CONX_SLAVE_ADDR, 0x0, &i2c_data[0], 1, 2);

		int elapsed_ms = 0;

		while (elapsed_ms < REPLY_POLL_TIMEOUT_MSEC)
		{
			// only read the first word and check the reply bit
			i2cRead(CONX_SLAVE_ADDR, 0x0, &i2c_data[0], 1, 2);

			if (cmd->reply==1)
				break;
			sys_mdelay(REPLY_POLL_INTERVAL_MSEC);
			elapsed_ms += REPLY_POLL_INTERVAL_MSEC;
		}

		if (cmd->reply==1)
		{
			if(cmd->num_32b_words > 0)
			{
				i2cRead(CONX_SLAVE_ADDR, 0x8, &i2c_data[2], cmd->num_32b_words, 2);
			}
			if (cmd->num_32b_words >= 0)
				return SENDCMD_RC_SUCCESS;
			else
				return SENDCMD_RC_CMD_ERROR;
		}
		return SENDCMD_RC_REPLY_TIMEOUT;
	}

	int GgecI2cCtrl::getFwVersion (Command *cmd) {

		int ret_val = SENDCMD(cmd, APP_ID_CTRL, CMD_GET(CONTROL_APP_VERSION));

		if (ret_val<0) {
            printf("LEDDemo failed to get FW version.");
		}

		printf("LEDDemo version number: %d.%d.%d.%d\n",cmd->data[0], cmd->data[1], cmd->data[2], cmd->data[3]);
		//alexaClientSDK::sampleApp::ConsolePrinter::simplePrint(
		//	"LEDDemo version number: " + cmd->data[0] + "." + cmd->data[1] + "." + cmd->data[2] + "." + cmd->data[3]);
		return(0);
	}

	#define  MDOAT_CMD_TRACKERS           0x0040
	float GgecI2cCtrl::getAngleOfArrival4mic(Command *cmd) {

		float angle_of_arrival, mag, max_mag = 0.0;
		for (int i = 0; i < 4; i++)
		{ /* Looping through the number of trackers */
			int ret_val = SENDCMD(cmd, (APP_ID_CAPT | 57), CMD_GET(MDOAT_CMD_TRACKERS), i);

			if (ret_val<0)
			{
				fprintf(stderr, "ERROR:CMD_GET(MDOAT_CMD_TRACKERS) command failed with error: %08x\n", ret_val);
				return (float)ret_val;
			}

			if (cmd->num_32b_words != 3 && cmd->num_32b_words != 4)
			{
				fprintf(stderr, "ERROR:CMD_GET(MDOAT_CMD_TRACKERS) command has wrong number of return values: %d\n", cmd->num_32b_words);
				return (float)MODULE_RC_CMD_DATA_SIZE_WRONG;
			}
			mag = ((float)((long)cmd->data[1]))/(1<<23);
			max_mag = (max_mag < mag) ? mag : max_mag;
			if (max_mag == mag){
				angle_of_arrival = 2.0*((float)((long)cmd->data[0]))/(1<<23);
			}
		}
		return angle_of_arrival;

	}

	void GgecI2cCtrl::ledSetState(int ledState, int dirIdx) {

		//char buf[NUMPIXELS*3+2];
		//buf[0] = 0x0;
		//buf[1] = ledState;
		//buf[2] = dirIdx;
		char buf[3];
		buf[0] = 0x0;
		buf[1] = ledState;
		buf[2] = dirIdx;
		i2cNbytesWrite(AVR_SLAVE_ADDR, (unsigned char *)&buf[0], 3);

		return;

	}

	void GgecI2cCtrl::ledCtrlIntfImpl(int ledState, unsigned char num) {
		if(USE_I2C_DEV) {
			int index;
			Command cmd = {0};
			float angle_of_arrival;

			switch (ledState) {

				case GGEC_LISTENING:
				case GGEC_ACTIVE_LISTENING:

#ifdef KWD_DSP
					angle_of_arrival = getVoiceDirection();
#else
//					getFwVersion(&cmd);
					angle_of_arrival = getAngleOfArrival4mic(&cmd);
#endif
					if (angle_of_arrival >= -360.0) {
                        LOG(INFO) << "The angle of arrival (degrees): " << angle_of_arrival;
						index = (int) ((angle_of_arrival * NUM_OF_LED) / 360.0);
#ifdef KWD_DSP
						index = DSPC_DIR_OFFSET - index;
#else
						index = CONXT_DIR_OFFSET -index;
#endif
						if (index >= NUM_OF_LED) {
                            index -= NUM_OF_LED;
                        }
                        LOG(INFO) << "index = " << index;
						ledSetState(ledState, index);
					}

					break;
				case GGEC_VOLUME_LED_SHOW:
				case GGEC_BREATH:
				case GGEC_BREATH_3TIMES:
					ledSetState(ledState, num);
					break;
				default:
					ledSetState(ledState, 0);
					break;

			}
		}
	}

	int GgecI2cCtrl::i2cNbytesWrite(unsigned char slave_addr, unsigned char *val, int len)
	{
		int ret = 0;
		struct i2c_rdwr_ioctl_data packets;
		struct i2c_msg messages;
		int i;

		packets.nmsgs = 1;
		packets.msgs = &messages;

		messages.addr = slave_addr;
		messages.flags = 0;         //write
		messages.len = len;     //数据长度
		//发送数据
		messages.buf = (unsigned char *)malloc(len+1);
		if (NULL == messages.buf)
		{
			ret = -1;
			free(messages.buf);
			return ret;
		}

		//messages.buf[0] = reg;
		for (i = 0; i < len; i++)
		{
			//messages.buf[1+i] = val[i];
			messages.buf[i] = val[i];
		}

		std::unique_lock<std::mutex> lock{m_mutex};
		ret = ioctl(m_i2cFd, I2C_RDWR, (unsigned long)&packets);//写数据
		lock.unlock();
		if (ret < 0){
            LOG(ERROR) << "i2cNbytesWrite write error!";
			return -1;
		}

		return ret;
	}

	void GgecI2cCtrl::sys_mdelay(unsigned int ms_delay)
	{
		usleep(ms_delay*1000);
	}

	void GgecI2cCtrl::i2cClose() {
		if(m_i2cFd > 0)
			close(m_i2cFd);
		return;
	}

	void GgecI2cCtrl::transmit_registers(unsigned char addr, cfg_reg *r, int n)
	{
		if(USE_I2C_DEV) {
			int i = 0;
			while (i < n) {
				switch (r[i].command) {
					case CFG_META_SWITCH:
						// Used in legacy applications.  Ignored here.
						break;
					case CFG_META_DELAY:
						break;
					case CFG_META_BURST:
						i2cNbytesWrite(addr, (unsigned char *) &r[i + 1], r[i].param);
						i += (r[i].param + 1) / 2;
						break;
					default:
						i2cNbytesWrite(addr, (unsigned char *) &r[i], 2);
						break;
				}
				i++;
			}
			return;
		}
	}

	int GgecI2cCtrl::getVoiceDirection(){
        if (m_dbusConnect == nullptr) {
            LOG(ERROR) << "getVoiceDirection m_dbusConnect NULL";
            return 0;
        }
        DBusMessage *msg;
        DBusMessage *reply;
        int result;
        int funRes;
        DBusError dberr;

        dbus_error_init(&dberr);
        msg = dbus_message_new_method_call(bus::CONNECT_AVSCLIENT_NAME, bus::CONNECT_AVSCLIENT_PATH,
                                           bus::CONNECT_AVSCLIENT_NAME, bus::METHOD_GET_VOICE_DIRECTION);

        if (msg == NULL) {
            LOG(ERROR) << "getVoiceDirection msg NULL";
            return 0;
        }

        dbus_message_append_args(msg, DBUS_TYPE_INVALID);
        reply = dbus_connection_send_with_reply_and_block(m_dbusConnect, msg, 100, &dberr);

        if (!reply) {
            LOG(ERROR) << "couldn't getVoiceDirection:" << dberr.message;
            dbus_message_unref(msg);
            return 0;
        } else {
            if (!dbus_message_get_args(reply, &dberr, DBUS_TYPE_INT32, &funRes, DBUS_TYPE_INT32, &result,
                                       DBUS_TYPE_INVALID)) {
                LOG(ERROR) << "getVoiceDirection get_args error: " << dberr.message;
                dbus_message_unref(reply);
                dbus_message_unref(msg);
                return 0;
            }
            dbus_message_unref(reply);
            dbus_message_unref(msg);
            if (funRes==1) {
                LOG(INFO) << "getVoiceDirection success degree:" << result;
                return result;
            } else {
                LOG(WARNING) << "getVoiceDirection fun fail";
                return -1;
            }
        }
	}

}
}
