//
// Created by z on 18-1-24.
//


#include <easylogging++.h>
#include "SpeakerManager.h"


namespace GGEC {
    namespace a113 {
        namespace speaker {
            using namespace alexaClientSDK::sampleApp;

            SpeakerManager::SpeakerManager(std::shared_ptr<GgecI2cCtrl> i2cCtrl, const std::string & filePath)
                    : m_ggecSqlite(std::unique_ptr<ggecSqlite>(new ggecSqlite(filePath))),
                      m_ggecI2cForPa(std::move(i2cCtrl)) {
                m_ggecI2cForPa->tas5754Init();
            }

            bool SpeakerManager::setVolume(const int volume) {
#ifdef HardVolumeReg

                volume = volume + GGEC_VOLUME_TOTAL_MAP;
            //snd_mixer_selem_get_playback_volume_range(_pMixerElement, &min, &max);
            //value = volume * max / 100;
            //printf( "get playback volume range: %ld ~ %ld, set value %ld, volume %d\n", min,max, value, volume);
            //if(value < 0) {
            //    return false;
            //}

			if(!_pMixerElement){
				return false;
			}

			if(!_pMixerElement1){
				return false;
			}

            snd_mixer_selem_set_playback_volume_all(_pMixerElement, volume);
			snd_mixer_selem_set_playback_volume_all(_pMixerElement1, volume);
#endif
                return m_ggecSqlite->modifyVolume(volume);
            }

            bool SpeakerManager::adjustVolume(const int adjustVolume, int * currentVolume) {

                if(getIsSpeakerMute()){
                    muteSpeaker(false);
                }
                int s_currentVolume = getCurrentVolume();

                LOG(INFO) << "before adjust,volume ="<<s_currentVolume;

                if(s_currentVolume < 10 && adjustVolume < 0){//modify by ghf
                    s_currentVolume=0;
                }
                else{
                    s_currentVolume += adjustVolume;
                }

                if(s_currentVolume > 100) {
                    s_currentVolume = 100;
                } else if(s_currentVolume < 0){
                    s_currentVolume = 0;
                }

                LOG(INFO) << "after adjust,volume ="<<s_currentVolume;

                *currentVolume = s_currentVolume;
                return setVolume(s_currentVolume);
            }

            bool SpeakerManager::muteSpeaker(const bool mute) {

                if(mute == getIsSpeakerMute()) {

                    LOG(INFO) << "set the same mute state";
                } else {
                    LOG(INFO) << "set the diff mute state";
                    cfg_reg tas5754_mute_registers[] = {
                            {0x3, 0x00},
                    };
                    if(mute == true) {
                        tas5754_mute_registers[0].value = 0x11;
                    }
                    m_ggecI2cForPa->transmit_registers(TAS5754_SLAVE_ADDR, tas5754_mute_registers,
                                                sizeof(tas5754_mute_registers)/sizeof(tas5754_mute_registers[0]));
                }
                return true;
            }

            int SpeakerManager::getCurrentVolume() {
                /*TODO*/
                return m_ggecSqlite->getVolume();
            }

            bool SpeakerManager::getIsSpeakerMute() {
                unsigned int regCurrMute;
                m_ggecI2cForPa->i2cRead(TAS5754_SLAVE_ADDR, 0x3, &regCurrMute, 1, 1);
                if((regCurrMute & 0x11) == 0x11)		// 0x11 is mute state, reference chip datasheet.
                    return true;
                else
                    return false;
            }

            bool SpeakerManager::getMicIsMute() {
                return m_ggecSqlite->getMuteState()==1;
            }

            bool SpeakerManager::handleSetMicMute(const int isMute) {
                if (m_ggecSqlite) {
                    return m_ggecSqlite->modifyMuteState(isMute);
                }

                return false;
            }
        }
    }
}