//
// Created by z on 18-1-24.
//

#ifndef GGEC_A113_EVENT_MANAGER_GGECKEYMANAGER_H
#define GGEC_A113_EVENT_MANAGER_GGECKEYMANAGER_H

#include <thread>

namespace GGEC {
    namespace a113 {
        namespace key {
            class GGECKeyManager {

            public:
                GGECKeyManager(void(*fp)(int keyCode, int pressTime));
                bool keyInit(const std::string keyDevPath = "/dev/input/event0");

                void do_aip_key_thread();

            private:
                int m_keyFd;
                std::thread aip_key_thread;

                void (*call_key)(int keyCode, int pressTime);
            };
        }
    }
}


#endif //GGEC_A113_EVENT_MANAGER_GGECKEYMANAGER_H
