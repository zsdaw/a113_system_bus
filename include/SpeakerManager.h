//
// Created by z on 18-1-24.
//

#ifndef GGEC_A113_EVENT_MANAGER_SPEAKERMANAGER_H
#define GGEC_A113_EVENT_MANAGER_SPEAKERMANAGER_H

#include "ggecI2cCtrl.h"
#include "sql/ggecSqlite.h"
#include <memory>

namespace GGEC {
    namespace a113 {
        namespace speaker {
            class SpeakerManager {

            public :
                SpeakerManager(std::shared_ptr<GgecI2cCtrl> i2cCtrl, const std::string & filePath);

                bool setVolume(const int volume);

                bool adjustVolume(const int adjustVolume, int * currentVolume);

                bool muteSpeaker(const bool mute);

                int getCurrentVolume();

                bool getIsSpeakerMute();

                bool getMicIsMute();

                bool handleSetMicMute(const int isMute);

            private:
                std::unique_ptr<alexaClientSDK::sampleApp::ggecSqlite> m_ggecSqlite;
                std::shared_ptr<GgecI2cCtrl> m_ggecI2cForPa;
            };

        }
    }
}

#endif //GGEC_A113_EVENT_MANAGER_SPEAKERMANAGER_H
