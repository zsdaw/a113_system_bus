//
// Created by z on 18-1-24.
//

#ifndef GGEC_A113_EVENT_MANAGER_LEDMANAGER_H
#define GGEC_A113_EVENT_MANAGER_LEDMANAGER_H

#include "timer/Timer.h"
#include "ggecI2cCtrl.h"
#include <memory>


namespace GGEC {
    namespace a113 {
        namespace led {

            static const char GGEC_LED_RESET = 0;
            static const char GGEC_ORANGE_LOADING = 1;
            static const char GGEC_LISTENING  = 2;
            static const char GGEC_ACTIVE_LISTENING = 3;
            static const char GGEC_THINKING = 4;
            static const char GGEC_SPEAKING = 5;
            static const char GGEC_MIC_OFF = 6;
            static const char GGEC_SYS_ERROR = 7;
            static const char GGEC_NET_UNCONNECTED = 8;
            static const char GGEC_NET_UNCONNECTED1 = 9;
            static const char GGEC_NET_UNCONNECTED2 = 10;
            static const char GGEC_VOLUME_LED_SHOW = 11;
            static const char GGEC_BREATH = 12;
            static const char GGEC_BREATH_3TIMES = 13;
            static const char GGEC_MIC_ON = 14; // for local use
            static const char GGEC_WELCOME_TIPS = 15; // for local use
            static const char GGEC_TRY_BACK_TO_IDLE = 16; // for local use
            static const char GGEC_FULL_WHITE = 100;

            class LedManager {

            public:

                LedManager(std::shared_ptr<GgecI2cCtrl> i2cCtrl);
                void setAVSState(char avsState, unsigned char subState);
                inline void setMicState(bool isIsMute){
                    m_isMicMute = isIsMute;
                }

            private:
                void ledCtrlIntf(int ledState, unsigned char num=0);
                void reset();
                bool tryBackToIdle(bool needSetBack = false);


                alexaClientSDK::avsCommon::utils::timing::Timer m_delayTimer;
                bool delayCall(int millisecond);
                void ledCtrlIntfImpl(int ledState, unsigned char num = 0);

                std::shared_ptr<GgecI2cCtrl> m_ggecI2cForLed;
                std::shared_ptr<GgecI2cCtrl> m_ggecI2cForPa;

                uint8_t m_notificationState = 0x00;
                bool m_isMicMute = false;
                int m_isSpeakingState = 0;

                int m_lastLedShowType = 0;
                int m_lastLedShowNum = 0;
            };
        }
    }
}


#endif //GGEC_A113_EVENT_MANAGER_LEDMANAGER_H
