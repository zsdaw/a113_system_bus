/*------------------------------------------------------------------------------
  Copyright (C) 2011-2015 Conexant Systems Inc.
  All rights reserved.

  CONEXANT SYSTEMS, INC. CONFIDENTIAL AND PROPRIETARY

  The information contained in this source code file
  is strictly confidential and proprietary to Conexant Systems, Inc.
  ("Conexant")

  No part of this file may be possessed, reproduced or distributed, in
  any form or by any means for any purpose, without the express written
  permission of Conexant Systems Inc.

  Except as otherwise specifically provided through an express agreement
  with Conexant that governs the confidentiality, possession, use
  and distribution of the information contained in this file, CONEXANT
  PROVIDES THIS INFORMATION "AS IS" AND MAKES NO REPRESENTATIONS OR
  WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
  WARRANTY OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
  TITLE OR NON-INFRINGEMENT, AND SPECIFICALLY DISCLAIMS SUCH WARRANTIES
  AND REPRESENTATIONS.  IN NO EVENT WILL CONEXANT BE LIABLE FOR ANY DAMAGES
  ARISING FROM THE USE OF THE INFORMATION CONTAINED IN THIS FILE.
--------------------------------------------------------------------------------

  File Name: cmds.h

  Description: The Command definitions

  Created Feb 4, 2015

-------------------------------------------------------------------------------*/
#ifndef CMDS_H_
#define CMDS_H_

/* control_ex.h */
typedef enum {
  CONTROL_APP_VERSION               =  3,
  CONTROL_APP_EXEC_FILE             =  4,
  CONTROL_APP_FW_UPGD               = 33,
  SOS_RESOURCE                      = 47,
  CONTROL_MGR_TUNED_MODES           = 85,
  CONTROL_MGR_CREATE_FILE         = 117,
  CONTROL_MGR_OPEN_FILE           = 118,
  CONTROL_MGR_OPEN_FILE_AT_BLK    = 119,
  CONTROL_MGR_CLOSE_FILE          = 120,
  CONTROL_MGR_DELETE_FILE         = 121,
  CONTROL_MGR_RW_FILE             = 122,
  CONTROL_MGR_STAT_FILE           = 123,
  CONTROL_MGR_CREATE_FILE_AT_BLK  = 124,
  CONTROL_MGR_DELETE_FILE_AT_BLK  = 125,
} ControlAppCommandCode;

#define CONTROL_APP_GET_VERSION             	CMD_GET(CONTROL_APP_VERSION)
#define CONTROL_MGR_READ_FILE  CMD_GET(CONTROL_MGR_RW_FILE)
#define CONTROL_MGR_WRITE_FILE CMD_SET(CONTROL_MGR_RW_FILE)


#endif // CMDS_H_
