//
// Created by lianghb on 2017/6/29.
//

#ifndef APPLED_CAPEHOST_H
#define APPLED_CAPEHOST_H
#include "command.h"
#include "capehost_env.h"

/*Error codes For FW download*/
#define ERRNO_NOERR                 0
#define ERRNO_SRC_FILE_NOT_EXIST    101
#define ERRNO_WRITE_FILE_FAILED     102
#define ERRNO_INVALID_DATA          103
#define ERRNO_CHECKSUM_FAILED       104
#define ERRNO_FAILED                105
#define ERRNO_INVALID_PARAMETER     106
#define ERRNO_NOMEM                 107
#define ERRNO_I2CFUN_NOT_SET        108
#define ERRNO_UPDATE_MEMORY_FAILED  109
#define ERRNO_DEVICE_NOT_RESET      110
#define ERRNO_DEVICE_OUT_OF_CONTROL 111
#define ERRNO_UPDATE_EEPROM_FAILED  112
#define ERRNO_INVALID_BOOTLOADER    113
#define ERROR_INVALID_IMAGE         114
#define ERROR_WAITING_RESET_TIMEOUT 115
#define ERROR_LOAD_IMG_TIMEOUT      116
#define ERROR_STATE_FATAL           117
#define ERROR_CRC_CHECK_ERROR       118
#define ERROR_I2C_ERROR             119
#define ERROR_UART_ERROR            120
#define ERROR_INVALID_ENCAPSULATION_HDR 121

#pragma warning(disable:4127) //conditional expression is constatnt
#define MODULE_RC_CMD_DATA_SIZE_WRONG    (-1024-34) // Wrong value of int32_size_of_data

#define APP_ID_SOS  ID('S','O','S',' ')
#define APP_ID_CTRL ID('C','T','R','L')
#define APP_ID_CAPT ID('C','A','P','T')

//int SendCmd_i2c   (Command *cmd);
//
//#if (!defined CAPE_ENV) || (CAPE_ENV != MCU_BUILD)
//int SendCmd0 (Command *cmd);
//int SendCmd1 (Command *cmd, uint32_t  app_module_id, uint32_t  command_id, uint32_t num_32b_words, ...);
//#endif

#if defined(_MSC_VER)
#define LEFT_PAREN (
#define RIGHT_PAREN )
#define NARGS(...) (_NARGN LEFT_PAREN __VA_ARGS__##_GRANN, _GRANN RIGHT_PAREN)
#define SENDCMD(cmd, app_module_id, command_id, ...) \
    SendCmd1(cmd, (uint32_t)(app_module_id), (uint32_t)(command_id), NARGS(__VA_ARGS__), __VA_ARGS__)
#elif defined(__GNUC__)

#define NARGS(...) (sizeof((int[]){0,##__VA_ARGS__})/sizeof(int)-1)

#define SENDCMD(cmd,app_module_id, command_id, ...) \
       SendCmd1(cmd,app_module_id, command_id, NARGS (__VA_ARGS__), ##__VA_ARGS__)
#else
#define NARGS(...) \
              (sizeof(#__VA_ARGS__) == sizeof("")?0:_NARG_(__VA_ARGS__, _GRANN))

#define _NARG_(...) _NARGN(__VA_ARGS__)
#define SENDCMD(cmd, app_module_id, command_id, ...) \
    SendCmd1(cmd, (uint32_t)(app_module_id), (uint32_t)(command_id), NARGS(__VA_ARGS__), __VA_ARGS__)
#endif

//inline int SendCmd_i2c   (Command *cmd)
//{
//    int num_32b_words = cmd->num_32b_words;
//
//    cmd->num_32b_words = (cmd->command_id&CMD_GET(0)) ? MAX_COMMAND_SIZE : num_32b_words;
//
//    unsigned int *i2c_data = (unsigned int *)cmd;
//    int size = num_32b_words + 2;
//
//    // write words 1 to N-1 , to addresses 4 to 4+4*N-1
//    i2c_write (0x4, &i2c_data[1], size-1);
//
//    // write word 0 to address 0
//    i2c_write (0x0, &i2c_data[0], 1);
//
//    int elapsed_ms = 0;
//
//    while (elapsed_ms < REPLY_POLL_TIMEOUT_MSEC)
//    {
//        // only read the first word and check the reply bit
//        i2c_read (0x0, &i2c_data[0], 1);
//
//        if (cmd->reply==1)
//            break;
//        sys_mdelay(REPLY_POLL_INTERVAL_MSEC);
//        elapsed_ms += REPLY_POLL_INTERVAL_MSEC;
//    }
//
//    if (cmd->reply==1)
//    {
//        if(cmd->num_32b_words > 0)
//        {
//            i2c_read (0x8, &i2c_data[2],cmd->num_32b_words);
//        }
//        if (cmd->num_32b_words >= 0)
//            return SENDCMD_RC_SUCCESS;
//        else
//            return SENDCMD_RC_CMD_ERROR;
//    }
//    return SENDCMD_RC_REPLY_TIMEOUT;
//}
//
//inline int SendCmd0 (Command *cmd)
//{
//	return SendCmd_i2c(cmd);
//}
//
//inline int SendCmd1 (Command *cmd, uint32_t  app_module_id, uint32_t  command_id, uint32_t num_32b_words, ...)
//{
//  va_list     args;
//  uint32_t    n         ;
//
//  va_start (args , num_32b_words) ;
//
//  // at least two words of header
//  if (num_32b_words > MAX_COMMAND_SIZE)
//  {
//    va_end (args) ;
//    return (SENDCMD_RC_INVALID_ARG_NUM);
//  }
//  cmd->num_32b_words = (command_id&CMD_GET(0)) ? MAX_COMMAND_SIZE : num_32b_words;
//  cmd->command_id    = command_id;
//  cmd->reply         = 0;
//  cmd->app_module_id = app_module_id;
//
//  for (n = 0 ; n < num_32b_words ; n++)
//  {
//    cmd->data[n] = va_arg(args, int32_t);
//  }
//  va_end (args) ;
//
//  return SendCmd0(cmd);
//}

//int i2c_init (int, int, char *);
//int i2c_close (void);
//int i2c_write(int address, unsigned int *buffer, int size);
//int i2c_read(int address, unsigned int *buffer, int size);
////void led_light(int idx);
//void led_set_state(int state, int index);
//int get_fw_version (Command *cmd);
//float get_angle_of_arrival_4mic(Command *cmd);

#endif //APPLED_CAPEHOST_H
