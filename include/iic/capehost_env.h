/*------------------------------------------------------------------------------
  Copyright (C) 2010-2016 Conexant Systems Inc.
  All rights reserved.

  CONEXANT SYSTEMS, INC. CONFIDENTIAL AND PROPRIETARY

  The information contained in this source code file
  is strictly confidential and proprietary to Conexant Systems, Inc.
  ("Conexant")

  No part of this file may be possessed, reproduced or distributed, in
  any form or by any means for any purpose, without the express written
  permission of Conexant Systems Inc.

  Except as otherwise specifically provided through an express agreement
  with Conexant that governs the confidentiality, possession, use
  and distribution of the information contained in this file, CONEXANT
  PROVIDES THIS INFORMATION "AS IS" AND MAKES NO REPRESENTATIONS OR
  WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY IMPLIED
  WARRANTY OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
  TITLE OR NON-INFRINGEMENT, AND SPECIFICALLY DISCLAIMS SUCH WARRANTIES
  AND REPRESENTATIONS.  IN NO EVENT WILL CONEXANT BE LIABLE FOR ANY DAMAGES
  ARISING FROM THE USE OF THE INFORMATION CONTAINED IN THIS FILE.
--------------------------------------------------------------------------------

  File Name:  capehost_env.h

  Description:  CAPE environment, general definitions and prototypes 
                for host side code
------------------------------------------------------------------------------*/

#ifndef CAPEHOST_ENV_H_
#define CAPEHOST_ENV_H_

/*------------------------------------------------------------------------------
  The following macros should be used with each function's definition
------------------------------------------------------------------------------*/
#define GLOBAL
#define STATIC              static

/*------------------------------------------------------------------------------
  The following definitions should be used for each parameter in a
  function's parameters list
------------------------------------------------------------------------------*/
#define IN_                 const
#define OUT
#define IO_

/*------------------------------------------------------------------------------
  Definitions of memory types - irrelevant for host side code
------------------------------------------------------------------------------*/
#ifndef __x86_64__
    #define __X
    #define __Y
#endif

/*------------------------------------------------------------------------------
  Definitions of basic types
------------------------------------------------------------------------------*/
typedef int                 BOOL      ;

#define FALSE               0
#define TRUE                1

#define INDEX_REAL          0
#define INDEX_IMAG          1

typedef union int32x2_tag
{
  long      x32[2] ;
  long long x64    ;
} int32x2_t ;

#define as_2xint(a,h,l)   l=(int)((a)&((uint32_t)0xFFFFFFFF)); h=(int)((a)>>32)
#define as_long_long(h,l) ((((int64_t)(h))<<32)|((uint32_t)(l)))

#define INT32_FROM_4BYTES(b0,b1,b2,b3) (b0 | (b1<<8) | (b2<<16) | (b3<<24))

#define INT_SIZE_OF(objtype,int_type) (((sizeof(objtype)-1)/sizeof(int_type))+1)

#define INT16_SIZE_OF(obj_type)         INT_SIZE_OF(obj_type, int16_t)
#define INT32_SIZE_OF(obj_type)         INT_SIZE_OF(obj_type, int32_t)
#define INT64_SIZE_OF(obj_type)         INT_SIZE_OF(obj_type, int64_t)

#define ARRAY_LENGTH(obj_type)          (sizeof(obj_type)/sizeof(obj_type[0]))

// A macro to align an array to n-int16_t boundary
#define INT16_ALIGN_ARRAY(uptr,ptr_type,n) \
  ((ptr_type)(((int32_t)(uptr) + (n)-1)  & (int32_t)(~((n)-1))))

#include <stdint.h>
#include <stdlib.h>

#if !defined _WIN32
#define __stdcall
#endif

typedef int64_t             dq31_t    ;
typedef int64_t             dq23_t    ;
typedef int32_t              q31_t    ;
typedef int32_t              q23_t    ;
typedef int32_t             q8_23_t   ;

#define Q31_T(val)   (((val)>=  1.f)?((1U<<31)-1):(((val)<  -1.f)?(1<<31U):((int32_t)((val)*2*(float)(1<<30)))<<0))
#define Q23_T(val)   (((val)>=  1.f)?((1U<<31)-1):(((val)<  -1.f)?(1<<31U):((int32_t)((val)*(float)(1<<23)))<<8))
#define Q8_23_T(val) (((val)>=256.f)?((1U<<31)-1):(((val)<-256.f)?(1<<31U):((int32_t)((val)*(float)(1<<23)))<<0))

#define FLOAT_FROM_Q31_T(val)   ((float)(val)*(0.5f/(float)(1<<30)))
#define FLOAT_FROM_Q23_T(val)   ((float)(val)*(0.5f/(float)(1<<30)))
#define FLOAT_FROM_Q8_23_T(val) ((val&0x80000000) ? (((float)(val)*(1.0f/(float)(1<<23))) - 512.f) : ((float)(val)*(1.0f/(float)(1<<23))))

#define ID(a,b,c,d)  ((((a)-0x20)<<8)|(((b)-0x20)<<14)|(((c)-0x20)<<20)|(((d)-0x20)<<26))
#define nID0		8
#define nID1		14
#define nID2		20
#define nID3		26
#define cID(c,n)	(((c)>>(n))&0x3f)+0x20
//#define pID(i)		cID(i,nID0),cID(i,nID1),cID(i,nID2),cID(i,nID3)

typedef struct Int32PtrTag {
  int32_t * ptr;
} Int32Ptr ;

typedef struct Int64PtrTag {
  int64_t * ptr;
} Int64Ptr ;

typedef struct Q23PtrTag {
  q23_t * ptr;
} Q23Ptr ;

typedef struct Q8_23PtrTag {
  q8_23_t * ptr;
} Q8_23Ptr ;

// The interval for reply bit polling and its timeout
#define REPLY_POLL_INTERVAL_MSEC     1
#define REPLY_POLL_TIMEOUT_MSEC   5000
#define RESET_INTERVAL_MSEC        100    //100 ms
#define MAX_RETRIES                  3
#define IMG_BLOCK_SIZE            2048
#define IMG_BLOCK_SIZE_CHUNK      4096

#ifndef NULL
#define NULL 0
#endif //#ifndef NULL

/*
set S  [binary format i 83]; # device->host: standing by for next packet
set T  [binary format i 84]; # device->host: bad packet. retry.
set F  [binary format i 70]; # device->host: fatal error

set R  [binary format i 82]; # host->device: packet transfer complete
set D  [binary format i 68]; # host->device: file transfer complete
set A  [binary format i 65]; # host->device: aborting transfer
set C  [binary format i 67]; # host->device: packet transfer complete
*/
enum { 
    TRANSFER_STATE_COMPLETE       = 'C',  //host->device: packet transfer complete. For bootloader.  //67
    TRANSFER_STATE_ABORT_TRANSFER = 'A',  // # host->device: aborting transfer  //65
    TRANSFER_STATE_FILE_DONE      = 'D',  //# host->device: file transfer complete //68
    TRANSFER_STATE_PACKET_READY   = 'R',  //# host->device: packet transfer complete  //82
    TRANSFER_STATE_FATAL          = 'F',  //# device->host: fatal error  //70
    TRANSFER_STATE_BAD_PACKET     = 'T',  //# device->host: bad packet. retry  //84
    TRANSFER_STATE_STANDBY        = 'S',  //#  device->host: standing by for next packet.  //83
    TRANSFER_STATE_TRANSFER_COMPLETE = 'O',  //#  device->host: Trnasfer complete, report CRC  //79
    TRANSFER_STATE_CLOSED_BY_DEVICE = 'X',  //#  device->host: Download window has been closed by device  //88
    TRANSFER_STATE_BAD_DEVICE = 'Z',  //#  device->host: Bad data reported by device  //90
}; 

#define MAKE_MAGIC_NUM(_w_,_x_,_y_,_z_) \
    ((uint32_t)(_z_) << 0)  | \
    ((uint32_t)(_y_) << 8)  | \
    ((uint32_t)(_x_) << 16) | \
    ((uint32_t)(_w_) << 24)  

#define  MSG_DONE_BOOTLOADER    0x01000000
#define  MSG_AUTO_RESETBOARD    0x02000000

#define DWN_PRI	  1	// download primary 'partition'
#define DWN_ALT	  2	// download alternate 'partition'
#define DWN_ALL	  4	// download alternate 'partition'

#endif  // CAPEHOST_ENV_H_
