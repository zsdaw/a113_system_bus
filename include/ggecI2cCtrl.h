#ifndef ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_SAMPLE_APP_GGEC_I2C_CTRL_H_
#define ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_SAMPLE_APP_GGEC_I2C_CTRL_H_

#include <mutex>
#include <iostream>
#include <stdio.h>  
#include <stdarg.h>
#include <linux/types.h>  
#include <fcntl.h>  
#include <unistd.h>  
#include <sys/types.h>  
#include <stdlib.h>  
#include <sys/ioctl.h>  
#include <errno.h>  
#include <assert.h>  
#include <string.h>  
#include <linux/i2c.h>  
#include <linux/i2c-dev.h>
#include "iic/command.h"
#include "iic/capehost.h"
#include "iic/cmds.h"
#include "iic/capehost_env.h"

#include <dbus/dbus.h>
#include "EventHeader.h"

#include <memory>


/* 
 * macro GGEC_USE_IIC_0_AND_2 and GGEC_USE_IIC_ONLY_2
   only one should be defined
   if iic0 and iic2 are used, define GGEC_USE_IIC_0_AND_2;
   if only iic2 is used, define GGEC_USE_IIC_ONLY_2;
   if no iic is used, both of them should not defined.
*/
#define	GGEC_USE_IIC_0_AND_2
//#define GGEC_USE_IIC_ONLY_2

namespace GGEC {
namespace a113 {
	
static const int DEF_MAX_I2C_WRITE_LEN = 120; 		/*MUST equal to or bigger than the 3rd parameters of SetupI2cWriteMemCallback.*/
static const int DEF_MAX_I2C_READ_LEN = 16;			/*MUST equal to or bigger than the 3rd parameters of SetupI2cReadMemCallback.*/
//static const int REPLY_POLL_INTERVAL_MSEC = 1;
static const int NUM_OF_LED = 12;
static const int CONXT_DIR_OFFSET = 14;
static const int DSPC_DIR_OFFSET = 13;
static const int AVR_SLAVE_ADDR = 0x10;
static const int CONX_SLAVE_ADDR = 0x41;
static const int TAS5754_SLAVE_ADDR = 0x4e;

static const char GGEC_LED_RESET = 0;
static const char GGEC_ORANGE_LOADING = 1;
static const char GGEC_LISTENING  = 2;
static const char GGEC_ACTIVE_LISTENING = 3;
static const char GGEC_THINKING = 4;
static const char GGEC_SPEAKING = 5;
//static const char GGEC_MIC_OFF = 6;
static const char GGEC_MIC_OFF = 9;
static const char GGEC_SYS_ERROR = 7;
static const char GGEC_NET_UNCONNECTED = 8;
static const char GGEC_NET_UNCONNECTED1 = 9;
static const char GGEC_NET_UNCONNECTED2 = 10;
static const char GGEC_VOLUME_LED_SHOW = 11;
static const char GGEC_BREATH = 12;
static const char GGEC_BREATH_3TIMES = 13;
static const char GGEC_MIC_ON = 14; // for local use
static const char GGEC_FULL_WHITE = 100;

typedef unsigned char cfg_u8;
typedef union {
	struct {
		cfg_u8 offset;
		cfg_u8 value;
	};
	struct {
		cfg_u8 command;
		cfg_u8 param;
	};
} cfg_reg;

class GgecI2cCtrl {
	
	public:
	
		//static std::unique_ptr<GgecI2cCtrl> create(const std::string i2cDevPath = "/dev/i2c-1");
		static std::shared_ptr<GgecI2cCtrl> create(const std::string i2cDevPath = "/dev/i2c-2");
		
		void ledCtrlIntfImpl(int ledState, unsigned char num);
		
		void tas5754Init();
		
		void transmit_registers(unsigned char addr, cfg_reg *r, int n);
		
		int i2cRead(int slaveAddr, int regAddr, unsigned int *buffer, int size, int g_bAddressOffset);
		
		//void setGgecSqlite(const std::shared_ptr<ggecSqlite> &ggecSqlite);
		
		//void tas5754VolSet();
		
		//int tas5754VolGet();
		void setBusConnect(DBusConnection * conn);
		int getVoiceDirection();

	private:

		
		bool i2cInit(std::string i2cDevPath);
		
		int i2cWrite(int slaveAddr, int regAddr, unsigned int *buffer, int size, int g_bAddressOffset = 2);
		
		int i2cNbytesWrite(unsigned char slave_addr, unsigned char *val, int len);
		
		void i2cClose();
		
		int getFwVersion (Command *cmd);
		
		float getAngleOfArrival4mic(Command *cmd);
		
		int SendCmd0 (Command *cmd);
		
		int SendCmd1 (Command *cmd, uint32_t  app_module_id, uint32_t  command_id, uint32_t num_32b_words, ...);
		
		int SendCmd_i2c (Command *cmd);
		
		void ledSetState(int ledState, int dirIdx);
		
		void sys_mdelay(unsigned int ms_delay);
		
		int m_i2cFd;
		
		int g_cbMaxI2cWrite;
		
		int g_cbMaxI2cRead;
		
		char* g_i2c_buf;
		

		std::mutex m_mutex;


//		int getVoiceDirection();
		DBusConnection *m_dbusConnect = nullptr;
};

	
}
}


#endif