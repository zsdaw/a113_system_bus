//
// Created by z on 18-1-24.
//

#ifndef GGEC_A113_EVENT_MANAGER_BUSEVENTMANAGER_H
#define GGEC_A113_EVENT_MANAGER_BUSEVENTMANAGER_H

#include <dbus/dbus.h>
#include <SpeakerManager.h>
#include <GGECKeyManager.h>
#include "EventHeader.h"
#include <LedManager.h>



namespace GGEC {
namespace a113 {
namespace bus {

class BusEventManager {

public:
    BusEventManager(const std::string & filePath);
    int run();
    void onkey(int keyCode, int pressTime);

private:
    DBusHandlerResult handleDBusMessage(DBusMessage *message);

    bool handleGetVolume(DBusMessage *message);
    bool handleSetVolume(DBusMessage *message);
    bool handleAdjustVolume(DBusMessage *message);

    bool handleGetSpeakerMute(DBusMessage *message);
    bool handleSetSpeakerMute(DBusMessage *message);

    bool handleGetMicMute(DBusMessage *message);
    bool handleSetMicMute(DBusMessage *message);

    bool handleSetAVSState(DBusMessage *message);

    bool sendSignal(DBusMessage* message);

    bool notifyVolumeChange(int volume, char who = W_System);
    bool notifyMicMuteStateChange(bool micMute, char who);
    bool notifyTriggerEvent();


    DBusConnection *m_dbusConnect = nullptr;
    std::shared_ptr<GgecI2cCtrl> m_ggecI2cForLed;
    speaker::SpeakerManager mSpeakerManager;
    key::GGECKeyManager mGgecKeyManager;
    led::LedManager mLedManager;
};

}
}
}


#endif //GGEC_A113_EVENT_MANAGER_BUSEVENTMANAGER_H
