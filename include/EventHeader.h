//
// Created by z on 18-1-24.
//

#ifndef GGEC_A113_EVENT_MANAGER_EVENTHEADER_H
#define GGEC_A113_EVENT_MANAGER_EVENTHEADER_H

namespace GGEC {
    namespace a113 {
        namespace bus {
            static const char STATE_SUCCESS = 1;
            static const char STATE_FAIL = 0;
            //        static const char STATE_;


            static const char W_AVS = 1;
            static const char W_MediaPlayer = 2;
            static const char W_System = 3;
            static const char W_Test = 4;

            static const char *CONNECT_NAME = "com.ggec.a113";
            static const char *CONNECT_PATH = "/com/ggec/a113";

            static const char *CONNECT_AVSCLIENT_NAME = "com.ggec.a113.avsclient";
            static const char *CONNECT_AVSCLIENT_PATH = "/com/ggec/a113/avsclient";

            /////// Signal
            static const char *SIGNAL_VOLUME_CHANGE = "SIGNAL_VOLUME_CHANGE"; // return current volume , set by who
            static const char *SIGNAL_SPEAKER_MUTE_CHANGE = "SIGNAL_SPEAKER_MUTE_CHANGE"; // return current speaker is mute , set by who
            static const char *SIGNAL_MIC_MUTE_CHANGE = "SIGNAL_MIC_MUTE_CHANGE"; // return current mic is mute , set by who
            static const char *SIGNAL_TRIGGRT_KEY_PRESS = "SIGNAL_TRIGGRT_KEY_PRESS"; // return trigger press, press time


            /////// Method ////////////
            //volume
            static const char *METHOD_GET_VOLUME = "METHOD_GET_VOLUME"; // return int 0 - 100
            static const char *METHOD_SET_VOLUME = "METHOD_SET_VOLUME"; // return
            static const char *METHOD_ADJUST_VOLUME = "METHOD_ADJUST_VOLUME"; // return
            //speaker mute
            static const char *METHOD_GET_SPEAKER_MUTE = "METHOD_GET_SPEAKER_MUTE";
            static const char *METHOD_SET_SPEAKER_MUTE = "METHOD_SET_SPEAKER_MUTE";
            //mic mute
            static const char *METHOD_GET_MIC_MUTE = "METHOD_GET_MIC_MUTE";
            static const char *METHOD_SET_MIC_MUTE = "METHOD_SET_MIC_MUTE";
            //avs led state
            static const char *METHOD_SET_AVS_STATE = "METHOD_SET_AVS_STATE"; // state ,num

            /////// AVS method ///////////
            static const char *METHOD_GET_VOICE_DIRECTION = "METHOD_GET_VOICE_DIRECTION";
        }
    }
}

#endif //GGEC_A113_EVENT_MANAGER_EVENTHEADER_H
