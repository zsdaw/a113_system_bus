#ifndef ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_SAMPLE_APP_GGEC_SQLITE_H_
#define ALEXA_CLIENT_SDK_SAMPLE_APP_INCLUDE_SAMPLE_APP_GGEC_SQLITE_H_

#include <sqlite3.h>
#include <string>
#include <iostream>
#include <mutex>

namespace alexaClientSDK {
namespace sampleApp {
	
class ggecSqlite {
	
public:
	
	ggecSqlite(const std::string & filePath);
	bool initializeSqlite(std::string storageFilePath);
	bool createDatabase(const std::string& filePath);
	bool open(const std::string& filePath);
	bool isOpen();
	void close();
	bool store(int volume, int isMute = 0);
	int  getVolume();
	int  getMuteState();
	bool modifyVolume(int volume);
	bool modifyMuteState(int isMute);
	bool erase();
	
private:
	bool loadHelper();
	sqlite3* m_dbHandle;
	int m_volume;
	int m_id;
	int m_isMute = 0;
	
	std::mutex m_mutex;
	
	
};	
	
	
}
}



#endif